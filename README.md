# Z_Motion data acquisition tools

## Présentation générale

Outils permettant la capture et l'enregistrement de données transmises par la
centrale inertielle Z_Motion développée dans le cadre du projet
[6TRON](https://6tron.io) porté
par le [Centre Aquitain des Technologies de l'Information et Electroniques](https://www.catie.fr) (CATIE).

## Installation

(Prodécure testée sous Ubuntu 18.04)

1/ Bibliothèques et outils relatifs à la communication bluetooth

```
sudo apt-get install bluez libgtk2.0-dev
```

2/ Installation de l'outil de packaging Python ```pipenv```

```
sudo apt-get install python3-pip

sudo python3 -m pip install virtualenv

sudo python3 -m pip install pipenv

```

3/ Récupération du code et création de l'environnement virtuel associé

```
git clone https://gitlab.com/workshop-ia/z-motion-data-acquisition.git

cd ./z-motion-data-acquisition/

sudo pipenv install
```

## Enregistrement des données transmises par le capteur

Le script ```record_data.py``` permet de capturer les données du z-motion. Deux commandes sont disponibles:
* `splitted`: capture plusieurs jeux de données en appuyant sur espace et enregistre des fichiers CSV séparés.
* `realtime`: envoie tous les éléments capturés dans une queue RabbitMQ. Un exemple de récupération des données est disponible sur le dépôt https://gitlab.com/workshop-ia/z-motion-data-from-rabbitmq.

### En fichiers séparés

La commande `splitted` permet de capturer, en une seule session, plusieurs
jeux de données et de les enregistrer sous la forme de fichiers CSV séparés.

Une fois le script lancé et l'objet connecté en bluetooth, appuyer sur la touche
[espace] pour commencer à enregistrer les données dans un fichier CSV.

Appuyer de nouveau sur [espace] pour interrompre l'enregistrement.

Un nouvel appui sur [espace] relancera l'enregistrement dans un nouveau fichier
CSV et ainsi de suite...

Une fois la session de travail terminée, appuyer sur la touche [x] pui
sur [ctrl+c] pour quitter le programme.


Utilisation :

```
usage: record_data.py splitted [-h] [--hci-device HCI_DEVICE]
                                    [--output-dir OUTPUT_DIR]
                                    [--files-prefix FILES_PREFIX]
                                    BLE_SENSOR_NAME
```

- HCI_DEVICE : numéro de l'interface bluetooth (par défaut, 0)
- OUTPUT_DIR : répertoire dans lequel seront enregistrés tous les fichiers
capturés durant la session
- FILES_PREFIX : préfixe des fichiers CSV correspondant aux différentes captures
- BLE_SENSOR_NAME : nom de l'objet Z_Motion (exemple : "6TRON BLE Sensor")

Exemple :

```
sudo pipenv run python data_acquisition/record_data.py splitted --output-dir ~/recorded_data/one/ --files-prefix one_ "6TRON BLE Sensor"
```

Enregistre une série de fichiers CSV dans le répertoire ~/recorded_data/one/

Ces fichiers seront respectivement nommés : one_0.csv, one_1.csv, one_2.csv...

Rappel : attention, la commande précédente doit être lancée en sudo, sinon il
ne sera pas possible de se connecter au capteur !

### Envoie de données à RabbitMQ

La commande `realtime` permet de renvoyer toutes les données du z-motion vers une queue RabbitMQ.

```bash
usage: record_data.py realtime [-h] [--hci-device HCI_DEVICE]
                                    [--hostname HOSTNAME]
                                    BLE_SENSOR_NAME
```

L'exemple suivant envoie les données vers une queue RabbitMQ locale:

```bash
sudo pipenv run python data_acquisition/record_data.py realtime --hostname localhost "6TRON BLE Sensor"
```
