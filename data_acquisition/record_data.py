# coding: utf-8
#
# Copyright (c) 2018, CATIE
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""BLE Service Forwarder"""

from bluepy import btle
from google.protobuf.internal.decoder import _DecodeVarint32
from pb.sensordata_pb2 import Quaternion, SensorData, Triple
from struct import pack, unpack
from threading import RLock, Thread
from time import sleep
import argparse
import csv
import google.protobuf
import paho.mqtt.publish as publish
import sys, termios, tty, os, time
import logging
import pika
import json

SCAN_LOCK = RLock()
STOP_ALL_THREADS = False


class BleScanner():

    # BLE Advertising Data Types definitions
    # See https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile
    _AD_TYPE_SHORTENED_LOCAL_NAME = 0x08
    _AD_TYPE_COMPLETE_LOCAL_NAME = 0x09

    def __init__(self, hci_device=0):
        self._hci_device = hci_device

    def scan(self, name, scan_timeout=5.0):
        # Scan peripherals and return address of specified peripheral
        print("Scanning BLE peripherals...\r")
        scanner = btle.Scanner(self._hci_device).withDelegate(BleScanner.ScanDelegate())
        devices = scanner.scan(scan_timeout)
        for device in devices:
            for (adtype, desc, value) in device.getScanData():
                if (adtype == self._AD_TYPE_SHORTENED_LOCAL_NAME
                        or adtype == self._AD_TYPE_COMPLETE_LOCAL_NAME):
                    print("BLE Peripheral: {}\r".format(value))
                    if value.strip("\0") == name: # BUG: We need to strip terminal '\0' for 6TRON peripherals
                        print("{} sensor found ({})\r".format(name, device.addr))
                        return device.addr
        return None


    class ScanDelegate(btle.DefaultDelegate):
        def __init__(self):
            btle.DefaultDelegate.__init__(self)

        def handleDiscovery(self, scan_entry, is_new_device, is_new_data):
            if is_new_device:
                print("Discovered device {}\r".format(scan_entry.addr))
            elif is_new_data:
                print("Received new data from {}\r".format(scan_entry.addr))


class BleSensor():

    def __init__(self, name, hci_device=0):
        self._name = name
        self._address = None
        self._hci_device = hci_device
        self._peripheral = None
        self._notifications = {}

    def connect(self, scan_timeout=5.0):

        # Get address
        scanner = BleScanner(hci_device=self._hci_device)
        self._address = scanner.scan(self._name)
        if not self._address:
            print("Sensor {} not found\r".format(self._name))
            return False

        # Connect to sensor
        print("Connecting to sensor ({})...\r".format(self._address))
        try:
            self._peripheral = btle.Peripheral(self._address, btle.ADDR_TYPE_RANDOM, self._hci_device)
            self._peripheral.withDelegate(BleSensor.SensorDelegate(self))
            print("Connected\r")
            return True

        except btle.BTLEException:
            print("Error connecting to sensor {}\r".format(self._name))
            return False

    def enable_notifications(self, characteristic, callback):
        try:
            uuid = btle.UUID(characteristic)
            handle = self._peripheral.getCharacteristics(uuid=uuid)[0].getHandle()
            self._peripheral.writeCharacteristic(handle + 1, pack("bb", 0x01, 0x00))
            self._notifications[uuid.binVal] = (handle, callback)

        except btle.BTLEException:
            print("Error enabling notifications for {}\r".format(self._name))

    def wait_for_notifications(self):
        try:
            while True:
                if self._peripheral.waitForNotifications(1.0):
                    continue

        except btle.BTLEException as e:
            print("Error waiting for notifications for {}: {}\r".format(self._name, str(e)))

    class SensorDelegate(btle.DefaultDelegate):
        def __init__(self, sensor):
            btle.DefaultDelegate.__init__(self)
            self._sensor = sensor

        def handleNotification(self, handle, data):
            for registered_handle, callback in self._sensor._notifications.values():
                if handle == registered_handle:
                    callback(data)


class CsvLogger(object):
    def __init__(self, output_dir_path, file_name_prefix):
        self._file_name_prefix = file_name_prefix
        self._output_dir_path = output_dir_path
        self._active = False
        self._epoch = 0
        self._csv_file_path_name = None

    def enable(self):
        self._active = True
        self._epoch += 1
        self.ensure_file_exists()

    def disable(self):
        self._active = False

    def is_active(self):
        return self._active

    def file_path(self):
        return self._csv_file_path_name

    def ensure_file_exists(self):
        csv_file_name = "%s%s.csv" % (self._file_name_prefix, self._epoch)
        csv_file_path_name = os.path.join(self._output_dir_path, csv_file_name)
        if not self._csv_file_path_name or self._csv_file_path_name != csv_file_path_name:
            self._csv_file_path_name = csv_file_path_name
            if not os.path.isfile(self._csv_file_path_name):
                with open(self._csv_file_path_name, "w") as csv_file:
                    csv_writer = csv.writer(csv_file)
                    csv_writer.writerow(
                        [
                            "timestamp",
                            "raw acceleration / x [m/s^2]",
                            "raw acceleration / y [m/s^2]",
                            "raw acceleration / z [m/s^2]",
                            "rotation speed / x [rad/s]",
                            "rotation speed / y [rad/s]",
                            "rotation speed / z [rad/s]",
                            "magnetic field strength / x [uT]",
                            "magnetic field strength / y [uT]",
                            "magnetic field strength / z [uT]",
                            "quaternion (w)",
                            "quaternion (x)",
                            "quaternion (y)",
                            "quaternion (z)"
                            ]
                        )

    def write(self, row):
        if self._active:
            with open(self._csv_file_path_name, "a") as csv_file:
                csv_writer = csv.writer(csv_file)
                csv_writer.writerow(row)


class RabbitMQLogger(object):
    def __init__(self, hostname):
        self.hostname = hostname
        self.channel = None

    def connect(self):
        successful_connection = False
        while not successful_connection:
            try:
                connection = pika.BlockingConnection(
                    pika.ConnectionParameters('localhost'))
                successful_connection = True
            except:
                logging.exception("exception")
                print("Rabbitmq not yet available.")
                time.sleep(1)  # Sleeps 1 second
        channel = connection.channel()
        channel.queue_declare(queue='zmotion')
        self.channel = channel

    def write(self, row):
        self.channel.basic_publish(
            exchange='',
            routing_key='zmotion',
            body=json.dumps(row)
        )


class MqttRelay(object):

    def __init__(self, broker="localhost", topic=""):
        self._broker = broker
        self._topic = topic

    def publish(self, data):
        try:
            publish.single(self._topic, data, hostname=self._broker)
        except OSError as error:
            print("Error publishing to MQTT broker on {}: {}\r".format(self._broker, error.strerror))


class KeystrokeHandler(Thread):

    def __init__(self, c, logger):
        Thread.__init__(self)
        self._c = c
        self._logger = logger

    def getch(self):
        # from : https://ubuntuforums.org/showthread.php?t=2394609
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)

        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

    def run(self):
        while True:
            if STOP_ALL_THREADS:
                break
            c = self.getch()
            if c == " ":
                if self._logger.is_active():
                    self._logger.disable()
                    print("Logger disabled.\r")
                else:
                    self._logger.enable()
                    print("Now logging to : %s\r" % self._logger.file_path())
            elif ord(c) in [3, 120, 122]:   # 3 : Ctrl+C, 120 : x, 122 : z
                self._logger.disable()
                print("Logger disabled.\r")
                print("\nPress ctrl+c to stop definitively...\n")
                break


class SensorHandler(Thread):

    def __init__(self, sensor_name, hci_device=0, logger=None):
        Thread.__init__(self)
        self._sensor_local_name = sensor_name
        self._sensor = BleSensor(sensor_name, hci_device)
        self._logger = logger
        self._rx_buffer = bytearray()

    def set_logger(self, logger):
        self._logger = logger

    def on_battery_level_notification(self, data):
        battery_level = int.from_bytes(data, byteorder="little")
        print("[{}] Battery level: {}\r".format(self._sensor_local_name, battery_level))

    def on_rx_stream_notification(self, data):
        try:
            self._rx_buffer.extend(data)

            # Get message size
            try:
                size, position = _DecodeVarint32(self._rx_buffer, 0)
                binary_message = self._rx_buffer[position:position+size]
            except google.protobuf.message.DecodeError:
                print("Not enough data received…\r")
                return
            except IndexError: # We may have missed the first part of the message
                print("[" + self._sensor_local_name + "]" + "Incorrect data received")
                self._rx_buffer.clear()
                return

            # At script startup we may not receive the first part of a BLE frame
            # In this case, we may compute an incorrect message size
            if size > 101 or size == 0: # Message maximum size is 101 bytes when serialized
                self._rx_buffer.clear()
                return

            # Check if message is complete
            if len(binary_message) < size:
                return

            if (size + position) < len(self._rx_buffer) : # Too much data, we certainly missed a part
                print("[" + self._sensor_local_name + "]" + "Incomplete data, discarding buffer")
                self._rx_buffer = bytearray(data) # try to restart from the last packet
                return

            # Check message is a valid protobuf message
            sensor_data = SensorData()
            try:
                sensor_data.ParseFromString(binary_message)
            except google.protobuf.message.DecodeError:
                print("{} Error decoding data: message incomplete?\r".format(self._sensor_local_name))
                self._rx_buffer.clear()
                return

            # Clear data
            self._rx_buffer.clear()

            # Log
            if self._logger:
                self._logger.write(
                    [
                        sensor_data.timestamp,
                        sensor_data.acceleration.x,
                        sensor_data.acceleration.y,
                        sensor_data.acceleration.z,
                        sensor_data.rotation_speed.x,
                        sensor_data.rotation_speed.y,
                        sensor_data.rotation_speed.z,
                        sensor_data.magnetic_field.x,
                        sensor_data.magnetic_field.y,
                        sensor_data.magnetic_field.z,
                        sensor_data.orientation.w,
                        sensor_data.orientation.x,
                        sensor_data.orientation.y,
                        sensor_data.orientation.z
                        ]
                    )

        except Exception as e:
            global STOP_ALL_THREADS
            STOP_ALL_THREADS = True
            raise

    def run(self):
        global SCAN_LOCK
        print("Starting Thread for sensor {}\r".format(self._sensor_local_name))
        # Main loop
        while True:
            # Connect to BLE sensor
            with SCAN_LOCK:
                while not self._sensor.connect():
                    print("Unable to connect to BLE sensor {}\r".format(self._sensor_local_name))

            # Enable desired notifications
            self._sensor.enable_notifications(0x2A19, self.on_battery_level_notification) # Battery Level characteristic
            self._sensor.enable_notifications("41260003d06dbaac701a4b0311982834", self.on_rx_stream_notification) # Stream service receive characteristic

            self._sensor.wait_for_notifications() # blocking until we disconnect
            print("Sensor {} Disconnected !\r".format(self._sensor_local_name))
            print("Reconnecting...\r")


class CommandRunner(object):

    @staticmethod
    def splitted(args):
        sensor_local_name = args.sensor_local_name
        hci_device = args.hci_device
        logger_output_dir_path = args.output_dir
        logger_files_prefix = args.files_prefix

        if not os.path.isdir(logger_output_dir_path):
            raise ValueError("This directory does not exist : %s" % logger_output_dir_path)

        logger = CsvLogger(logger_output_dir_path, logger_files_prefix)

        print("\nPress [space] to start / stop CSV logging...\n")
        print("Press [z] and [ctrl+c] to quit...\n")

        threads = []
        thread1 = SensorHandler(sensor_local_name, hci_device, logger=logger)
        threads.append(thread1)
        thread1.daemon = True
        thread1.start()
        thread2 = KeystrokeHandler("c", logger=logger)
        threads.append(thread2)
        thread2.daemon = True
        thread2.start()

        for thread in threads:
            thread.join()

    @staticmethod
    def realtime(args):
        sensor_local_name = args.sensor_local_name
        hci_device = args.hci_device
        hostname = args.hostname

        logger = RabbitMQLogger(hostname)
        logger.connect()
        thread1 = SensorHandler(sensor_local_name, hci_device, logger=logger)
        thread1.daemon = True
        thread1.start()
        thread1.join()


def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='either realtime or splitted')

    parser_splitted = subparsers.add_parser('splitted', help='save data to distinct files every the user ask for an acquisition')
    parser_splitted.add_argument("--output-dir", type=str, help="output dir path", required=True)
    parser_splitted.add_argument("--files-prefix", type=str, help="output log files prefix", required=True)
    parser_splitted.set_defaults(func=CommandRunner.splitted)

    parser_realtime = subparsers.add_parser("realtime", help="send all data to a RabbitMQ queue")
    parser_realtime.add_argument("--hostname", type=str, help="hostname of the RabbitMQ queue", required=True)
    parser_realtime.set_defaults(func=CommandRunner.realtime)

    parser.add_argument("sensor_local_name", metavar="BLE_SENSOR_NAME", help="sensor to connect to")
    parser.add_argument("--hci-device", type=int, default=0, help="HCI device to use")

    args = parser.parse_args()
    args.func(args)
    

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("\nExiting...\r")

    # Close stderr to get rid of broken pipe error message
    sys.stderr.close()
